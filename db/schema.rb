# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160206213929) do

  create_table "frames", force: :cascade do |t|
    t.integer  "game_id",                null: false
    t.integer  "throw1",     default: 0, null: false
    t.integer  "throw2"
    t.integer  "throw3"
    t.integer  "score",      default: 0, null: false
    t.integer  "offset",     default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "frames", ["game_id", "offset"], name: "index_frames_on_game_id_and_offset", unique: true

  create_table "games", force: :cascade do |t|
    t.integer  "instance_id",                        null: false
    t.string   "name",        limit: 32,             null: false
    t.integer  "score",                  default: 0, null: false
    t.integer  "offset",                 default: 0, null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "games", ["instance_id", "name"], name: "index_games_on_instance_id_and_name", unique: true

  create_table "instances", force: :cascade do |t|
    t.integer  "slot_id",                               null: false
    t.string   "name",          limit: 32,              null: false
    t.integer  "finalized",                default: 0,  null: false
    t.integer  "state_id",                 default: 10, null: false
    t.integer  "current_frame",            default: 0,  null: false
    t.integer  "current_game",             default: 0,  null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "instances", ["slot_id", "finalized", "state_id"], name: "index_instances_on_slot_id_and_finalized_and_state_id", unique: true

  create_table "lock_masters", force: :cascade do |t|
    t.string   "table_name", null: false
    t.integer  "table_id",   null: false
    t.string   "session_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "lock_masters", ["table_name", "table_id"], name: "index_lock_masters_on_table_name_and_table_id", unique: true

  create_table "slots", force: :cascade do |t|
    t.string   "name",                       null: false
    t.integer  "offset",                     null: false
    t.boolean  "invisible",  default: false, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "slots", ["name"], name: "index_slots_on_name", unique: true

end
