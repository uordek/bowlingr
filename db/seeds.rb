# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Slot.create(name: 'Game Slot 1', offset: 10, invisible: false)

Slot.create(name: 'Game Slot 2', offset: 20, invisible: false)

Slot.create(name: 'Game Slot 3', offset: 30, invisible: false)

Slot.create(name: 'Game Slot 4', offset: 40, invisible: false)