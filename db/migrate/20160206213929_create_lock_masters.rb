class CreateLockMasters < ActiveRecord::Migration
  def change
    create_table :lock_masters do |t|
      t.string :table_name, null: false
      t.integer :table_id, null: false
      t.string :session_id, null: true

      t.timestamps null: false
    end

    add_index :lock_masters, [:table_name, :table_id], :unique => true
  end
end
