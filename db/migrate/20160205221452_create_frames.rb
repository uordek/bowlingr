class CreateFrames < ActiveRecord::Migration
  def change
    create_table :frames do |t|
      t.integer :game_id, null: false
      t.integer :throw1, null: false, default: 0
      t.integer :throw2, null: true, default: nil
      t.integer :throw3, null: true, default: nil
      t.integer :score, null: false, default: 0
      t.integer :offset, null: false, default: 0

      t.timestamps null: false
    end

    add_index :frames, [:game_id, :offset], :unique => true
  end
end
