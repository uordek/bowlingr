class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.integer :instance_id, null: false
      t.string :name, null: false, limit: 32
      t.integer :score, null: false, default: 0
      t.integer :offset, null: false, default: 0

      t.timestamps null: false
    end

    add_index :games, [:instance_id, :name], :unique => true
  end
end