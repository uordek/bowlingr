class CreateSlots < ActiveRecord::Migration
  def change
    create_table :slots do |t|
      t.string :name, :null => false
      t.integer :offset, :null => false
      t.boolean :invisible, :null => false, :default => false

      t.timestamps null: false
    end

    add_index :slots, :name, :unique => true
  end
end