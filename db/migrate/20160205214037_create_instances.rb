class CreateInstances < ActiveRecord::Migration
  def change
    create_table :instances do |t|
      t.integer :slot_id, null: false
      t.string :name, null: false, limit: 32
      t.integer :finalized, null: false, default: 0
      t.integer :state_id, null: false, default: Instance::NEW
      t.integer :current_frame, null: false, default: 0
      t.integer :current_game, null: false, default: 0

      t.timestamps null: false
    end

    add_index :instances, [:slot_id, :finalized, :state_id], :unique => true 
  end
end