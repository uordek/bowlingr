class InstancesController < ApplicationController
  before_action :set_instance, only: [:show, :edit, :update, :close, :start, :add_game, :remove_game, :update_frame, :calculate]

  # GET /instances
  # GET /instances.json
  def index
    @instances = Instance.all
  end

  def calculate
    result= false
    error_str= ''
    lock= LockMaster.lock('instances', @instance.id, 'instance.calculate')
    begin
      save_list= @instance.calculate
      Instance.transaction do
        begin
          if save_list
            save_list.each do |o|
              o.save!
            end
          end
          result= true
        rescue Exception => ex
          logger.error "ERROR: InstanceCNT#calculate, #{ex}, #{@instance.inspect}"
          result= false
          error_str= ex.message
        end
      end
    ensure
      lock.clear if lock != nil
    end

    respond_to do |format|
      if result
        format.html { redirect_to @instance, notice: t('instance_calculated') }
        format.json { render :show, status: :ok, location: @instance }
      else
        format.html { redirect_to @instance, alert: "#{t('errors.instance_not_calculated')} #{error_str}" }
        format.json { render json: @instance.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_frame
    throw1= params[:throw1]
    throw2= params[:throw2]
    throw3= params[:throw3]

    result= false
    error_str= ''
    lock= LockMaster.lock('instances', @instance.id, 'instance.update_frame')
    begin
      save_list= @instance.update_frame(throw1, throw2, throw3)
      Instance.transaction do
        begin
          if save_list
            save_list.each do |o|
              o.save!
            end
          end
          @instance.save!
          result= true
        rescue Exception => ex
          logger.error "ERROR: InstancesController#update_frame, #{ex}, #{@instance.inspect}"
          result= false
          error_str= ex.message
        end
      end
    ensure
      lock.clear if lock != nil
    end

    respond_to do |format|
      if result
        format.html { redirect_to @instance, notice: t('instance_frame_updated') }
        format.json { render :show, status: :ok, location: @instance }
      else
        format.html { redirect_to @instance, alert: "#{t('errors.instance_frame_not_updated')} #{error_str}" }
        format.json { render json: @instance.errors, status: :unprocessable_entity }
      end
    end
  end

  def add_game
    game= Game.new()
    game.name= params[:name]
    if @instance.check_start == false
      redirect_to @instance, alert: t('errors.game_can_not_added_instance_started')
      return
    end

    lock= LockMaster.lock('instances', @instance.id, 'instance.add_game')
    begin
      game.offset= (@instance.games.maximum('offset')||0) + 1
    ensure
      lock.clear if lock != nil
    end
    @instance.games<< game

    respond_to do |format|
      if game.save
        format.html { redirect_to @instance, notice: "#{t('game_added')} (#{game.full_name})" }
        format.json { render :show, status: :created, location: game }
      else
        format.html { redirect_to @instance, alert: "#{t('errors.game_can_not_added')} #{game.errors.full_messages.join(',')}" }
        format.json { render json: game.errors, status: :unprocessable_entity }
      end
    end
  end

  def remove_game
    if @instance.check_close == false && @instance.check_start == false
      redirect_to @instance, alert: t('errors.game_can_not_removed_instance_closed')
      return
    end

    game_id= params[:game_id]
    result= @instance.games.destroy(Game.find(game_id))

    respond_to do |format|
      if result != nil
        format.html { redirect_to @instance, notice: "#{t('game_removed')} #{result[0].full_name}" }
        format.json { render :show, status: :deleted, location: game }
      else
        format.html { render :show, alert: t('errors.game_can_not_removed') }
        format.json { render json: game.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /instances/1
  # GET /instances/1.json
  def show
    @games= @instance.games.order('offset, name')
    @frames= Frame.where('game_id in (?)', @games.collect { |g| g.id }.flatten)

    @current_game_name= @games.select { |g| g.offset == @instance.current_game }[0].try(:name)
  end

  # GET /instances/new
  def new
    @instance = Instance.new
    @instance.slot_id= params[:slot_id]
    @instance.state_id= Instance::NEW
  end

  # GET /instances/1/edit
  def edit
  end

  # POST /instances
  # POST /instances.json
  def create
    @instance = Instance.new(instance_params)
    @instance.state_id= Instance::NEW

    respond_to do |format|
      if @instance.save
        format.html { redirect_to @instance, notice: t('instance_created') }
        format.json { render :show, status: :created, location: @instance }
      else
        format.html { render :new }
        format.json { render json: @instance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /instances/1
  # PATCH/PUT /instances/1.json
  def update
    respond_to do |format|
      if @instance.update(instance_params)
        format.html { redirect_to @instance, notice: t('instance_updated') }
        format.json { render :show, status: :ok, location: @instance }
      else
        format.html { render :edit }
        format.json { render json: @instance.errors, status: :unprocessable_entity }
      end
    end
  end

  def start
    result= false
    lock= LockMaster.lock('instances', @instance.id, 'instance.start')
    begin
      result= @instance.save if @instance.start
    ensure
      lock.clear if lock != nil
    end

    respond_to do |format|
      if result
        format.html { redirect_to @instance, notice: t('instance_started') }
        format.json { render :show, status: :created, location: @instance }
      else
        format.html { redirect_to @instance, alert: "#{t('errors.instance_not_started')} #{@instance.errors.full_messages.join(',')}" }
        format.json { render json: @instance.errors, status: :unprocessable_entity }
      end
    end
  end

  def close
    result= false
    lock= LockMaster.lock('instances', @instance.id, 'instance.close')
    begin
      result= @instance.save! if @instance.close
    rescue Exception => ex
      logger.error "ERROR: InstancesController#close, #{ex}, #{@instance.inspect}"
    ensure
      lock.clear if lock != nil
    end

    respond_to do |format|
      if result
        format.html { redirect_to @instance, notice: t('instance_closed') }
        format.json { render :show, status: :created, location: @instance }
      else
        format.html { redirect_to @instance, alert: "#{t('errors.instance_not_closed')} #{@instance.errors.full_messages.join(',')}" }
        format.json { render json: @instance.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_instance
      @instance = Instance.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def instance_params
      params.require(:instance).permit(:name, :slot_id, :current_game, :current_frame)
    end
end
