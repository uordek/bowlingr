class HomeController < ApplicationController
  
  def index
    @slots= Slot.with_instance.order(:offset)

    @instance_hash= {}
    Instance.where('state_id <> ? and updated_at > ?', Instance::NEW, 30.days.ago).order('updated_at DESC').each do |i|
      slot_id= i.slot_id
      if @instance_hash.key?(slot_id) == false
        @instance_hash[slot_id]= [i]
      else
        @instance_hash[slot_id]<< i
      end
    end
  end
end