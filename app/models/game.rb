class Game < ActiveRecord::Base
  belongs_to :instance
  has_many :frames

  validates :name, presence: true, length: { minimum: 3, maximum: 33}, :uniqueness => { :scope => :instance_id, :case_sensitive => false }, on: :create

  validates_numericality_of :score, :greater_than_or_equal_to => 0, :allow_nil => false
  validates_numericality_of :offset, :greater_than_or_equal_to => 0, :allow_nil => false

  validate :post_validate

  before_create :pre_create

  delegate :closed?, :to => :instance, :prefix => false

  MAX_FRAME_OFFSET = 9

  def full_name
    "#{self.offset}##{self.name}"
  end

  def post_validate
    errors.add :score, I18n.t('errors.game_already_closed') if self.closed?
  end

  def pre_create
    0.upto(MAX_FRAME_OFFSET).each do |offset|
      self.frames.build(offset: offset, score: 0, throw1: 0)
    end
  end

  # For ruby, I'd prefer non-recursive functions instead of recursive ones because of performance issues. 
  def calculate(frames = nil)
    frame_hash = {}
    if frames == nil
      Frame.where('game_id = ?', self.id).each do |frame|
        frame_hash[frame.offset] = frame
      end
    else
      frames.each do |frame|
        frame_hash[frame.offset] = frame
      end
    end

    # Last frame has a special calculation.
    last_frame = frame_hash[MAX_FRAME_OFFSET]
    if last_frame.strike? or last_frame.spare?
      last_frame.score = (last_frame.throw1||0) + (last_frame.throw2||0) + (last_frame.throw3||0)
    else
      last_frame.score = (last_frame.throw1||0) + (last_frame.throw2||0)
    end

    (MAX_FRAME_OFFSET - 1).downto(0).each do |index|
      current_frame = frame_hash[index]

      current_frame.score = (current_frame.throw1||0) + (current_frame.throw2||0)
      
      if current_frame.strike?
        next_frame = frame_hash[index+1]
        current_frame.score+= (next_frame.throw1||0)
        if next_frame.strike? && next_frame.offset != MAX_FRAME_OFFSET
          current_frame.score+= (frame_hash[index+2].throw1||0)
        else
          current_frame.score+= (next_frame.throw2||0)
        end
      elsif current_frame.spare?
        current_frame.score+= (frame_hash[index+1].throw1||0)
      end
    end
    all_frames = frame_hash.values
    total_score = 0
    all_frames.each do |f|
      total_score += (f.score||0)
    end
    self.score = total_score
    
    return all_frames
  end
end