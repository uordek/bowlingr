class Instance < ActiveRecord::Base
  belongs_to :slot
  has_many :games

  NEW = 10
  STARTED = 20
  CLOSED = 30
  STATE_NAME_LIST= {NEW => 'instance_state_new', STARTED => 'instance_state_started', CLOSED => 'instance_state_closed'}

  validates :name, presence: true, length: { minimum: 3, maximum: 32}, :uniqueness => { :scope => :slot_id, :case_sensitive => false }, :if => Proc.new { |m| m.name_changed? }
  validates_numericality_of :current_frame, :greater_than_or_equal_to => 0, :allow_nil => false
  validates_numericality_of :current_game, :greater_than_or_equal_to => 0, :allow_nil => false
  validates :slot_id, :uniqueness => { scope: [:finalized, :state_id], :case_sensitive => false }, :if => Proc.new { |m| m.state_id_changed? }

  validate :post_validate
  delegate :name, :to => :slot, :prefix => true

  def post_validate
    errors.add :base, I18n.t('errors.instance_already_closed') if self.closed?
  end

  def closed?
    self.state_id == CLOSED && self.state_id_was == CLOSED
  end

  def state_name
    I18n.t(STATE_NAME_LIST[self.state_id])
  end

  def calculate
    return false unless check_close

    frame_list= []
    self.games.each do |g|
      frame_list<< g.calculate(g.frames.order(:offset))
    end

    [frame_list, self.games].flatten
  end

  def check_start
    if self.state_id != NEW
      false
    else
      true
    end
  end

  def start
    unless self.check_start
      logger.error "ERROR: Instance#start, #{I18n.t('errors.instance_must_be_new')},#{self.inspect}"
      return false
    end

    if self.games.size == 0
      errors.add :base, I18n.t('errors.instance_can_not_start_without_player')
      return false
    end

    self.state_id = STARTED
    
    self.current_game= 1
    self.current_frame= 0
    true
  end

  def check_close
    if self.state_id != STARTED
      false
    else
      true
    end
  end

  def close
    return false unless check_close

    frames = []
    begin
      games.each do |game|
        frames << game.calculate
        logger.debug "DEBUG: Instance#close, Calculation completed for #{game.inspect}"
      end
    rescue Exception => ex
      logger.error "ERROR: Instance#close, #{ex}, #{self.inspect}"
      return false
    end
    
    self.state_id = CLOSED
    self.finalized = self.id

    (frames + games).flatten
  end

  def update_frame(throw1, throw2, throw3)
    if self.current_game == 0
      self.current_frame+= 0
      self.current_game+= 1
      return false
    end
    
    game= self.games.select { |g| g.offset == self.current_game }[0]
    frame= game.frames.select { |f| f.offset == self.current_frame }[0]

    frame.throw1= throw1
    frame.throw2= throw2
    frame.throw3= throw3

    if self.current_frame >= Game::MAX_FRAME_OFFSET
      next_games= self.games.order(:offset).select { |g| g.offset > self.current_game }
      if next_games.size != 0
        self.current_game= next_games[0].offset
      else
        self.current_game= 1
      end
      self.current_frame= 0
    else
      self.current_frame+= 1
    end
    [game, frame]
  end
end