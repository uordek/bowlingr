class LockMaster < ActiveRecord::Base

  def self.lock(table_name= "models", table_id= -1, session_id= "", retry_limit= 50, retry_sleep= 0.1)
    clear_locks = false
    retry_count = 0
    begin
      lock_master = self.create(:table_name => table_name, :table_id => table_id, :session_id => session_id)
    rescue
      if retry_count < retry_limit
        LockMaster.clear_expiries if retry_count == 0
        sleep(retry_sleep)
        retry_count += 1
        retry
      else
        nil
      end
    else
      lock_master
    end
  end

  def self.test(table_name= "models", table_id= -1)
    LockMaster.where('table_name = ? and table_id = ? and created_at > ?', table_name, table_id, 3.minutes.ago).first
  end

  def self.clear_expiries
    begin
      self.delete_all(["created_at < ?", 3.minutes.ago])
    rescue
    end
  end

  def clear
    LockMaster.clear(self.table_name, self.table_id)
  end

  def self.clear(table_name= "models", table_id= -1)
    return if table_name== nil || table_id== nil
    begin
      self.delete_all(["table_name = ? and table_id = ?", table_name, table_id])
    rescue
    end
  end
end