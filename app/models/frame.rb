class Frame < ActiveRecord::Base
  belongs_to :game

  MAX_SCORE = 10

  validates :offset, presence: true, :uniqueness => { :scope => :game_id }, on: :create

  validates_numericality_of :score, :greater_than_or_equal_to => 0, :allow_nil => false
  validates_numericality_of :throw1, :greater_than_or_equal_to => 0, :less_than_or_equal_to => MAX_SCORE, :allow_nil => false
  validates_numericality_of :throw2, :greater_than_or_equal_to => 0, :less_than_or_equal_to => MAX_SCORE, :allow_nil => true
  validates_numericality_of :throw3, :greater_than_or_equal_to => 0, :less_than_or_equal_to => MAX_SCORE, :allow_nil => true
  validates_numericality_of :offset, :greater_than_or_equal_to => 0, :allow_nil => false

  validate :post_validate
  delegate :closed?, :to => :game, :prefix => false

  def post_validate
    errors.add :base, I18n.t('errors.frame_already_closed') if self.closed?

    if self.throw2 != nil && self.throw2 > 0
      if (self.throw1 + self.throw2) > MAX_SCORE
        if self.offset != Game::MAX_FRAME_OFFSET || !self.strike?
          errors.add :base, I18n.t('errors.throw1_throw2_can_not_greater_than_max')
        end
      end
    end

    if self.throw3 != nil && self.throw3 > 0
      if self.offset == Game::MAX_FRAME_OFFSET
        if self.strike? == false && self.spare? == false
          errors.add :throw3, I18n.t('errors.throw3_only_used_in_strike_spare')
        end
      else
        errors.add :throw3, I18n.t('errors.throw3_only_used_in_last_frame')
      end
    end

    if self.strike? and self.throw2 != nil
      errors.add :base, I18n.t('errors.frame_is_strike') if self.closed?
    end
  end
  
  def strike?
    self.throw1 == MAX_SCORE
  end

  def spare?
    ((self.throw1||0) + (self.throw2||0)) == MAX_SCORE
  end
end