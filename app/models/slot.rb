class Slot < ActiveRecord::Base
  has_many :instances
  
  validates :name, presence: true, length: { minimum: 3, maximum: 33}, :uniqueness => { :case_sensitive => false }

  def self.with_instance
    slots= Slot.select('slots.*, i.id as instance_id0, i.name as instance_name0')
    slots= slots.joins('LEFT OUTER JOIN instances as i ON i.slot_id = slots.id AND i.state_id = 10')
    slots
  end

  def instance_name
    self.respond_to?('instance_name0') ? self.instance_name0 : I18n.t('errors.instance_not_found')
  end

  def instance_id
    self.respond_to?('instance_id0') ? self.instance_id0 : nil
  end
end