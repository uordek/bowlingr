require 'test_helper'

class InstanceTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "closed instance can not be change" do
    instance = instances(:closed)
    instance.name += "123"
    assert instance.save == false, instance.errors.inspect
  end

  test "start not available for started instances" do
    instance = instances(:started)
    assert instance.start == false, instance.errors.inspect
  end

  test "start not available for closed instances" do
    instance = instances(:closed)
    instance.start
    assert instance.save == false, instance.errors.inspect
  end

  test "start available for new instances" do
    instance = instances(:new)
    instance.start
    assert instance.save == true, instance.errors.inspect
  end

  test "close not available for new instances" do
    instance = instances(:new)
    assert instance.close == false, instance.errors.inspect
  end

  test "close available for started instances" do
    instance = instances(:started)
    assert instance.close != false, instance.errors.inspect
  end
end